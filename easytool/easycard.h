#ifndef EASYCARD_H
#define EASYCARD_H

#include <nfc/nfc-types.h>
#include <nfc/mifaretag.h>
#include "utils.h"

#define EASY_TT_MRT_ENTER	0x00
#define EASY_TT_BUS		0x01
#define EASY_TT_MRT_REENTER	0x80
#define EASY_TT_MRT_EXIT	0x11
#define EASY_TT_PURCHASE	0x20
#define EASY_TT_RECHARGE	0x30

extern const struct value_string easy_tt_names[];
extern const struct value_string taipei_mrt_stn_id[];

/* Block 0 of Sector 2 seems to contain manufacturing timestamp */
struct easy_sect2blk0 {
	uint8_t unknown[6];
	uint8_t timestamp[3];
	uint8_t unknown2[7];
} __attribute__ ((packed));

/* Block 2 of Sector 15 */
struct easy_sect15blk2 {
	uint8_t unknown[11];
	uint8_t day_of_month;
	uint8_t unknown2;	/* always 0x3d? */
	uint16_t sum_of_day;	/* sum of all shop purchases on a day */
	uint8_t unknown3;
} __attribute__ ((packed));

/* Sector 7 */
struct easy_sect7blk {
	uint8_t unknown[3];
	uint8_t value1;
	uint8_t station_code;
	uint8_t unknown2[5];
	uint8_t timestamp[3];
	uint8_t unknown3[3];
} __attribute__ ((packed));

/* storage of a transaction log record on the transponder itself */
struct easy_log_rec {
	uint8_t trans_id;
	uint8_t unknown;
	uint8_t timestamp[3];	/* seconds since January 1st 1970 / 256 */
	uint8_t trans_type;
	uint16_t amount;	/* transaction amount / value */
	uint16_t remaining;	/* remaining value on card _after_ trans */
	uint8_t unknown2;
	uint8_t station_code;	/* MRT station code */
	uint16_t reader_code;	/* unique code of RFID reader */
	uint8_t unknown3[2];
} __attribute__ ((packed));

/* functions for data format conversion */

time_t easy_timestamp2time(const uint8_t *easy_ts);
char *easy_asc_timestamp(const uint8_t *timestamp);

/* functions to make a change to individual portions of the card */

/* apply a delta (positive or negative) to a EasyCard log record */
int easy_update_log_rec(struct easy_log_rec *elr, int16_t delta);
/* apply a delta to the 'sum of day' record in Sector 15 Block 2 */
int easy_update_sum_of_day(struct easy_sect15blk2 *s15b2, int16_t delta);

/* functions to make a comprehensive change, leave all card state in
 * a consistent state */

/* positive value: make it more expensive.  negative: cheaper */
int easy_alter_last_purchase(mifare_tag *mft, int16_t delta);
/* positive value: make it more expensive.  negative: cheaper */
int easy_alter_last_recharge(mifare_tag *mft, int16_t delta);

/* functions to dump the transaction log */

/* dump a single log record */
void easy_dump_log_rec(const struct easy_log_rec *elr);
/* dump the entire transaction log */
void easy_dump_log(mifare_tag *mft);

#endif /* EASYCARD_H */
