/* A reverse-engineered implementation of the EasyCard data format */

/* (C) 2010 by Harald Welte <laforge@gnumonks.org>
 * All Rights Reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>

#include "utils.h"
#include "easycard.h"

/* Taipei MRT station name / ID mapping */
/* Result of http://web.trtc.com.tw/e/stationdetail.asp?ID=XX */
const struct value_string taipei_mrt_stn_id[] = {
	{ 7,	"Songshan Airport" },
	{ 8,	"Zhongshan Junior Highschool" },
	{ 9,	"Nanjing East Road" },
	{ 10,	"Zhongxiao Fuxing" },
	{ 11,	"Daan" },
	{ 12,	"Technology Building" },
	{ 13,	"Liuzhangli" },
	{ 14,	"Linguang" },
	{ 15,	"Xinhai" },
	{ 16,	"Wanfang Hospital" },
	{ 17,	"Wanfang Community" },
	{ 18,	"Muzha" },
	{ 19,	"Taipei Zoo" },
	{ 21,	"Dazhi" },
	{ 22,	"Jiannan Road" },
	{ 23,	"Xihu" },
	{ 24,	"Gangquian" },
	{ 25,	"Wende" },
	{ 26,	"Neihu" },
	{ 27,	"Dahu Park" },
	{ 28,	"Huzhou" },
	{ 29,	"Donghu" },
	{ 30,	"Nangang Software Park" },
	{ 31,	"Nangang Exhibition Center" },
	{ 32,	"Xiaobitan" },
	{ 33,	"Xindian" },
	{ 34,	"Xindian City Office" },
	{ 35,	"Quizhang" },
	{ 36,	"Dapinglin" },
	{ 37,	"Jingmei" },
	{ 38,	"Wanlong" },
	{ 39,	"Gongguan" },
	{ 40,	"Taipower Building" },
	{ 41,	"Guting" },
	{ 42,	"CKS Memorial Hall" },
	{ 43,	"Xiaonanmen" },
	{ 45,	"Dingxi" },
	{ 46,	"Yongan Market" },
	{ 47,	"Jingan" },
	{ 48,	"Nanshijiao" },
	{ 50,	"NTU Hospital" },
	{ 51,	"Taipei Main Station" },
	{ 53,	"Zhongshan" },
	{ 54,	"Shuanglian" },
	{ 55,	"Minquan West Road" },
	{ 56,	"Yuanshan" },
	{ 57,	"Jiantan" },
	{ 58,	"Shilin" },
	{ 59,	"Zhishan" },
	{ 60,	"Mingde" },
	{ 61,	"Shipai" },
	{ 62,	"Qilan" },
	{ 63,	"Qiyan" },
	{ 64,	"Beitou" },
	{ 65,	"Xinbeitou" },
	{ 66,	"Fuxinggang" },
	{ 67,	"Zhongyi" },
	{ 68,	"Guandu" },
	{ 69,	"Zhuwei" },
	{ 70,	"Hongshulin" },
	{ 71,	"Danshui" },
	{ 77,	"Yongning" },
	{ 78,	"Tucheng" },
	{ 79,	"Haishan" },
	{ 80,	"Far Eastern Hospital" },
	{ 81,	"Fuzhong" },
	{ 82,	"Banqiao" },
	{ 83,	"Xinpu" },
	{ 84,	"Jiangzicui" },
	{ 85,	"Longshan Temple" },
	{ 86,	"Ximen" },
	{ 88,	"Shandao Temple" },
	{ 89,	"Zhongxiao Xinsheng" },
	{ 91,	"Zhongxiao Dunhua" },
	{ 92,	"SYS Memorial Hall" },
	{ 93,	"Taipei City Hall" },
	{ 94,	"Yongchun" },
	{ 95,	"Houshanpi" },
	{ 96,	"Kunyang" },
	{ 97,	"Nangang" },
	{ 0,	NULL }
};

/* Easycard Transaction Type names */
const struct value_string easy_tt_names[] = {
	{ EASY_TT_MRT_ENTER,	"Enter MRT" },
	{ EASY_TT_BUS,		"Bus ride" },
	{ EASY_TT_MRT_REENTER,	"ReEnter MRT" },
	{ EASY_TT_MRT_EXIT,	"Leave MRT" },
	{ EASY_TT_PURCHASE,	"Shop Purchase" },
	{ EASY_TT_RECHARGE,	"Recharge" },
	{ 0,			NULL }
};
