#ifndef UTILS_H
#define UTILS_H

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

struct value_string {
	unsigned int value;
	const char *str;
};
const char *get_value_string(const struct value_string *vs, uint32_t val);

#endif
