/* A reverse-engineered implementation of the EasyCard data format */

/* (C) 2010 by Harald Welte <laforge@gnumonks.org>
 * All Rights Reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

/* System includes */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <netinet/in.h>

/* libnfc includes */
#include <nfc/nfc-types.h>
#include <nfc/mifaretag.h>

#include "mifare_classic.h"

/* Easycard specific includes */
#include "easycard.h"

#define VERSION	"0.05"
#define COPYRIGHT 						\
	"EasyTool "VERSION" (C) 2010 by Harald Welte <laforge@gnumonks.org>\n"	\
	"This is FREE SOFTWARE with ABSOLUTELY NO WARRANTY\n\n"	\
	"Use of this software is authorized for RESEARCH PURPOSE ONLY!\n\n" 

enum mode {
	MODE_DUMP_MFACC,
	MODE_DUMP,
	MODE_RECHARGE,
	MODE_PURCHASE,
};

struct {
	int fd;
	unsigned long size;
	mifare_tag *mft;
	enum mode mode;
} global;

static void dump_acc_bits(const struct acc_bits_parsed *abp)
{
	uint8_t block;

	for (block = 0; block < 4; block++) {
		printf("\tBlock %u: %x (%u %u %u)\n", block, abp->block[block],
			abp->block[block] & ABP_C1 ? 1 : 0,
			abp->block[block] & ABP_C2 ? 1 : 0,
			abp->block[block] & ABP_C3 ? 1 : 0);
	}
}

static void dump_mfcl(mifare_tag *mft)
{
	unsigned int sect;

	mifare_block_manufacturer *manuf = &mft->amb[0].mbm;

	printf("UID %x, ATQA %x\n", ntohl(*((uint32_t *) manuf->abtUID)),
		ntohs(*((uint16_t *) manuf->abtATQA)));

	for (sect = 0; sect < 16; sect++) {
		unsigned int block_base = sect * 4;
		uint8_t *access_bits = mft->amb[block_base+3].mbt.abtAccessBits;
		struct acc_bits_parsed abp;

		printf("Sector %02u (base: 0x%02x) Access bits: 0x%08x\n", sect,
			sect*4*16, ntohl(*((uint32_t *) access_bits)));
		mfcl_parse_acc_bits(&abp, access_bits);
		dump_acc_bits(&abp);
	}
}

static void dump_easycard(mifare_tag *mft)
{
	mifare_block_manufacturer *manuf = &mft->amb[0].mbm;
	struct mfcl_value_block *val =
			(struct mfcl_value_block *) mft->amb[8].mbd.abtData;
	struct easy_sect2blk0 *b2s0 =
			(struct easy_sect2blk0 *) mft->amb[1*4+0].mbd.abtData;
	struct easy_sect15blk2 *b15s2 =
			(struct easy_sect15blk2 *) mft->amb[15*4+2].mbd.abtData;
	struct easy_sect7blk *mrt_enter =
			(struct easy_sect7blk *) mft->amb[7*4+2].mbd.abtData;
	struct easy_sect7blk *mrt_leave =
			(struct easy_sect7blk *) mft->amb[7*4+1].mbd.abtData;
	uint32_t uid = *((uint32_t *) manuf->abtUID);

	/* dump the header */
	printf("EasyCard UID 0x%08x (%u)\n", ntohl(uid), uid);
	printf("Date of manufacture: %s\n",
		easy_asc_timestamp(b2s0->timestamp));
	printf("Current Balance: %5u NTD\n", val->value);
	printf("Sum of all purchases on day %u (of month): %u NTD\n",
		b15s2->day_of_month, b15s2->sum_of_day);

	printf("Last MRT station entered: %s (%s)\n", 
		get_value_string(taipei_mrt_stn_id, mrt_enter->station_code),
		easy_asc_timestamp(mrt_enter->timestamp));
	printf("Last MRT station left: %s (%s)\n", 
		get_value_string(taipei_mrt_stn_id, mrt_leave->station_code),
		easy_asc_timestamp(mrt_leave->timestamp));

	/* dump the transaction log */
	printf("\nTransaction Log:\n");
	easy_dump_log(mft);
}

static void print_help(void)
{
	printf( " -a --dump-access-bits\t\tDump the MIFARE access bits\n"
		" -r --alter-recharge DELTA\tAlter the last recharge\n"
		" -p --alter-purchase DELTA\tAlter the last purchase\n"
		" -h --help\t\t\tPrint this help message\n");
}

int main(int argc, char **argv)
{
	struct stat st;
	int delta = 0;
	int option_index = 0;
	int rc;
	int prot, flags = O_RDONLY;

	global.mode = MODE_DUMP;

	printf(COPYRIGHT);

	while (1) {
		int c;
		static struct option long_options[] = {
			{ "dump-access-bits", 0, 0, 'a' },
			{ "alter-recharge", 1, 0, 'r' },
			{ "alter-purchase", 1, 0, 'p' },
			{ "help", 0, 0, 'h' },
			{ 0, 0, 0, 0 }
		};

		c = getopt_long(argc, argv, "r:p:ha",
				long_options, &option_index);
		if (c == -1)
			break;

		switch (c) {
		case 'a':
			global.mode = MODE_DUMP_MFACC;
			break;
		case 'r':
			global.mode = MODE_RECHARGE;
			flags = O_RDWR;
			delta = atoi(optarg);
			break;
		case 'p':
			global.mode = MODE_PURCHASE;
			flags = O_RDWR;
			delta = atoi(optarg);
			break;
		case 'h':
			print_help();
			exit(0);
			break;
		}
	};

	if (argc <= optind) {
		fprintf(stderr, "ERROR: You must specify the file name of "
			"a mifare dump file (.mfd)\n");
		print_help();
		exit(2);
	}

	global.fd = open(argv[optind], flags);
	if (global.fd < 0) {
		perror("Error opening the MFD file");
		exit(1);
	}
	if (fstat(global.fd, &st) < 0) {
		perror("Error stat()ing the MFD file");
		exit(1);
	}
	global.size = st.st_size;

	prot = PROT_READ;
	if (flags == O_RDWR)
		prot |= PROT_WRITE;

	global.mft = mmap(NULL, global.size, prot, MAP_SHARED, global.fd, 0);
	if (!global.mft) {
		perror("Error mmap()ing the MFD file");
		exit(1);
	}

	switch (global.mode) {
	case MODE_DUMP_MFACC:
		dump_mfcl(global.mft);
		break;
	case MODE_DUMP:
		dump_easycard(global.mft);
		break;
	case MODE_RECHARGE:
		rc = easy_alter_last_recharge(global.mft, delta);
		break;
	case MODE_PURCHASE:
		rc = easy_alter_last_purchase(global.mft, delta);
		break;
	}

	munmap(global.mft, global.size);
	close(global.fd);
	exit(0);
}
