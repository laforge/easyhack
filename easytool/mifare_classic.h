#ifndef EASY_MFCL_H
#define EASY_MFCL_H

#include <stdint.h>

#define ABP_C1	0x04
#define ABP_C2	0x02
#define ABP_C3	0x01

struct acc_bits_parsed {
	uint8_t block[4];
};

void mfcl_parse_acc_bits(struct acc_bits_parsed *abp, uint8_t *acc_bits);

/* Mifare classic VALUE BLOCK */
struct mfcl_value_block {
	uint32_t value;		/* value in NTD */
	uint32_t value_inv;	/* bit-inverted copy of value in NTD */
	uint32_t value_backup;	/* backup copy of value in NTD */
	uint8_t addr[4];
} __attribute__ ((packed));

struct mfcl_addr {
	uint8_t sector;
	uint8_t block;
};

/* apply a delta (positive or negative) to a Mifare Classic VALUE block */
int mfcl_update_value_block(struct mfcl_value_block *mvb, int32_t delta);

#endif
