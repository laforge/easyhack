#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "mifare_classic.h"

void mfcl_parse_acc_bits(struct acc_bits_parsed *abp, uint8_t *acc_bits)
{
	uint8_t c1, c2, c3;
	uint8_t block;

	memset(abp, 0, sizeof(*abp));

	c1 = acc_bits[1] >> 4;
	c2 = acc_bits[2] & 0xf;
	c3 = acc_bits[2] >> 4;

	printf("C1 = %x, C2 = %x, C3 = %x\n", c1, c2, c3);

	for (block = 0; block < 4; block++) {
		uint8_t testbit = 1 << block;
		if (c1 & testbit)
			abp->block[block] |= ABP_C1;
		if (c2 & testbit)
			abp->block[block] |= ABP_C2;
		if (c3 & testbit)
			abp->block[block] |= ABP_C3;
	}
}

/* apply a delta (positive or negative) to a Mifare Classic VALUE block */
int mfcl_update_value_block(struct mfcl_value_block *mvb, int32_t delta)
{
	int64_t sum = mvb->value + delta;

	if (sum > 0xffffffff || sum < 0)
		return -ERANGE;

	mvb->value = sum;
	mvb->value_backup = mvb->value;
	mvb->value_inv = ~sum;

	return 0;
}
